CREATE DATABASE  IF NOT EXISTS `farmers` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `farmers`;
-- MySQL dump 10.13  Distrib 8.0.31, for macos12 (x86_64)
--
-- Host: localhost    Database: farmers
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_bids`
--

DROP TABLE IF EXISTS `product_bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_bids` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `bid_amount` decimal(10,2) DEFAULT NULL,
  `bid_qty` int DEFAULT NULL,
  `bid_qty_type` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `productId_userId` (`product_id`,`user_id`),
  KEY `bid_user_id_idx` (`user_id`),
  KEY `bid_product_id_idx` (`product_id`),
  CONSTRAINT `bid_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bid_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_bids`
--

LOCK TABLES `product_bids` WRITE;
/*!40000 ALTER TABLE `product_bids` DISABLE KEYS */;
INSERT INTO `product_bids` VALUES (3,1,2,60.03,1,'kg',NULL,NULL),(5,1,3,56.00,1,'kg','2022-12-27 18:38:00','2022-12-27 18:38:00'),(6,6,2,105.00,1000,'kg','2022-12-27 20:21:08','2022-12-27 20:21:08'),(7,2,2,40.00,1,'kg','2022-12-27 20:39:40','2022-12-27 20:39:40');
/*!40000 ALTER TABLE `product_bids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_image` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_price` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_qty` int DEFAULT NULL,
  `qty` int DEFAULT NULL,
  `base_qty_type` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_type` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `isActive` int DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_product_id_idx` (`user_id`),
  CONSTRAINT `user_product_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'POTATO','https://cdn.mos.cms.futurecdn.net/iC7HBvohbJqExqvbKcV3pP.jpg','50',1,2,'kg','ton',1,1,'2022-12-25 09:17:39','2022-12-25 09:17:39'),(2,'POTATO','https://cdn.mos.cms.futurecdn.net/iC7HBvohbJqExqvbKcV3pP.jpg','50.4',1,2,'kg','ton',1,1,'2022-12-25 09:19:48','2022-12-25 09:19:48'),(3,'ONION','https://5.imimg.com/data5/ANDROID/Default/2021/10/FM/ER/YP/108695390/img-20211022-wa0015-jpg-500x500.jpg','50.4',1,2,'kg','ton',1,1,'2022-12-25 09:20:04','2022-12-25 09:20:04'),(4,'Aubergine','https://cdn.mos.cms.futurecdn.net/G6o7bF6AbmDWWu67rJZ39c-415-80.jpg','100',1,2,'kg','Ton',1,1,'2022-12-27 15:36:01','2022-12-27 15:36:01'),(6,'Pumpkin','https://upload.wikimedia.org/wikipedia/commons/5/5c/FrenchMarketPumpkinsB.jpg','100',1,2,'kg','Ton',5,1,'2022-12-27 20:17:25','2022-12-27 20:17:25'),(7,'Watermelon','https://fsi.colostate.edu/wp-content/uploads/2020/08/watermelon-2636_1920-1170x659.jpg','50',1,3,'kg','Ton',5,1,'2022-12-27 20:38:46','2022-12-27 20:38:46');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_rating`
--

DROP TABLE IF EXISTS `user_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_rating` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `rating_user` int DEFAULT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `rating_user_UNIQUE` (`user_id`,`rating_user`),
  KEY `user_rating_id_idx` (`user_id`),
  KEY `rating_user_id_idx` (`rating_user`),
  CONSTRAINT `user_rating_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rating`
--

LOCK TABLES `user_rating` WRITE;
/*!40000 ALTER TABLE `user_rating` DISABLE KEYS */;
INSERT INTO `user_rating` VALUES (1,1,1,5,'2022-12-25 12:13:49.000000'),(2,1,1,7,'2022-12-25 12:14:08.000000'),(3,1,4,74,'2022-12-25 12:14:15.000000');
/*!40000 ALTER TABLE `user_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_type` (
  `id` int NOT NULL,
  `type` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_type`
--

LOCK TABLES `user_type` WRITE;
/*!40000 ALTER TABLE `user_type` DISABLE KEYS */;
INSERT INTO `user_type` VALUES (1,'farmer'),(2,'retailer');
/*!40000 ALTER TABLE `user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int DEFAULT NULL,
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `user_type_id_idx` (`type`),
  CONSTRAINT `user_type_id` FOREIGN KEY (`type`) REFERENCES `user_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'farmer1','test',NULL,1,'test address','999999999','$2a$10$fnZzAVobWfq8sHVGiBbjWucHGCKmGCLZjx5gQvFkIR2p2LKMCiJve',NULL),(2,'retailer1','test-retailer-1',NULL,2,'test retailer address 1','23565154','$2a$10$fnZzAVobWfq8sHVGiBbjWucHGCKmGCLZjx5gQvFkIR2p2LKMCiJve',NULL),(3,'retailer2','test-retailer-2',NULL,2,'test retailer addres 2','264564646','$2a$10$fnZzAVobWfq8sHVGiBbjWucHGCKmGCLZjx5gQvFkIR2p2LKMCiJve',NULL),(4,'retailer3','test-ret-3',NULL,2,'test ret add 3','64567896556','$2a$10$fnZzAVobWfq8sHVGiBbjWucHGCKmGCLZjx5gQvFkIR2p2LKMCiJve',NULL),(5,'farmer2','test farmer 2',NULL,1,'test farmer address','9879461','$2a$10$fnZzAVobWfq8sHVGiBbjWucHGCKmGCLZjx5gQvFkIR2p2LKMCiJve',NULL),(6,'admin','admin',NULL,1,'admin','1352235','$2a$10$fnZzAVobWfq8sHVGiBbjWucHGCKmGCLZjx5gQvFkIR2p2LKMCiJve',NULL),(8,'admin1','admin','one',1,'test','+91543563','$2a$10$fnZzAVobWfq8sHVGiBbjWucHGCKmGCLZjx5gQvFkIR2p2LKMCiJve','2022-12-25 08:06:53.000000');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-27 20:51:14
