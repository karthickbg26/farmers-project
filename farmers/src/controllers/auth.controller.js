import User from '../models/user.model.js';
import { hash as hashPassword, compare as comparePassword } from '../utils/password.js';
import { generate as generateToken } from '../utils/token.js';

export const signup = (req, res) => {
    const { username, firstname, lastname, type, address, contact, password } = req.body;
    const hashedPassword = hashPassword(password.trim());

    const user = new User(username.trim(), firstname.trim(), lastname, type, address.trim(), contact.trim(), hashedPassword);

    User.create(user, (err, data) => {
        if (err) {
            res.status(500).send({
                status: "error",
                message: err.message
            });
        } else {
            const token = generateToken(data.id);
            res.status(201).send({
                status: "success",
                statusCode:201,
                data: {
                    token,
                    data
                }
            });
        }
    });
};

export const signin = (req, res) => {
    const { username, password } = req.body;
    User.findByUsername(username.trim(), (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    status: 'error',
                    message: `User with username ${username} was not found`
                });
                return;
            }
            res.status(500).send({
                status: 'error',
                message: err.message
            });
            return;
        }
        if (data) {
            if (comparePassword(password.trim(), data.password)) {
                const {password, ...resData} = data
                const token = generateToken(resData);
                res.status(200).send({
                    status: 'success',
                    statusCode:200,
                    data: {
                        token,
                        user_id: data.id,
                        firstname: data.firstname,
                        lastname: data.lastname,
                        username: data.username,
                        userType: data.type
                    }
                });
                return;
            }
            res.status(401).send({
                status: 'error',
                statusCode:401,
                message: 'Incorrect password'
            });
        }
    });

}