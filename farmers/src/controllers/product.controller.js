import Product from '../models/product.model.js';
import { decode } from '../utils/token.js';

export const create = (req, res) => {

    const { product_name, product_image, base_price, qty, base_qty, base_qty_type, qty_type, user_id } = req.body;

    const product = new Product(product_name, product_image, base_price, qty, base_qty, base_qty_type, qty_type, user_id);
    Product.create(product, (err, data) => {
        if (err) {
            res.status(500).send({
                status: "error",
                message: err.message
            });
        } else {
            res.status(200).send({
                status: "success",
                statusCode: 200,
                data: {
                    data
                }
            });
        }
    });
};

export const findAll = (req, res) => {
    const decoded = decode(req.headers.authorization.split(' ')[1]);
    const cb = (err, data) => {
        if (err) {
            res.status(500).send({
                status: "error",
                message: err.message
            });
        } else {
            res.status(200).send({
                status: "success",
                statusCode: 200,
                data: {
                    data
                }
            });
        }
    }
    decoded.type === 1 ? Product.findAllByUser(decoded.id, cb) : Product.findAll(cb);
};

export const find = (req, res) => {
    const { id } = req.params;
    Product.find(id, (err, data) => {
        if (err) {
            res.status(500).send({
                status: "error",
                message: err.message
            });
        } else {
            res.status(200).send({
                status: "success",
                statusCode: 200,
                data: {
                    data
                }
            });
        }
    });
};

export const productBid = (req, res) => {
    const { productId } = req.params;
    Product.productBid(productId, (err, data) => {
        if (err) {
            console.log('err', err);
            res.status(500).send({
                status: "error",
                message: err.message
            });
        } else {
            res.status(200).send({
                status: "success",
                statusCode: 200,
                data: {
                    data
                }
            });
        }
    });
};

export const createBid = (req, res) => {

    const payload = req.body;

    Product.createBid(payload, (err, data) => {
        if (err) {
            res.status(500).send({
                status: "error",
                message: err.message
            });
        } else {
            res.status(200).send({
                status: "success",
                statusCode: 200,
                data: {
                    data
                }
            });
        }
    });
};