import User from '../models/user.model.js';

export const findAllUsers = (req, res) => {


    User.findAllUsers((err, data) => {
        if (err) {
            res.status(500).send({
                status: "error",
                message: err.message
            });
        } else {
            res.status(200).send({
                status: "success",
                statusCode: 200,
                data: {
                    data
                }
            });
        }
    });
};



export const createRating = (req, res) => {
    const ratingPayload = req.body
    User.createRating(ratingPayload, (err, data) => {
        if (err) {
            res.status(500).send({
                status: "error",
                message: err.message
            });
        } else {
            res.status(200).send({
                status: "success",
                statusCode: 200,
                data: {
                    data
                }
            });
        }
    });
};

export const averageRating = (req, res) => {
    const { userId } = req.params;
    User.averageRating(userId, (err, data) => {
        if (err) {
            res.status(500).send({
                status: "error",
                message: err.message
            });
        } else {
            res.status(200).send({
                status: "success",
                statusCode: 200,
                data: {
                    data
                }
            });
        }
    });
};