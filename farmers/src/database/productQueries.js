const createProduct = `INSERT INTO products VALUES(null,?, ?, ?, ?,?,?,?,?,1,NOW(),NOW())`;
const getAllProduct = `select * from products`;
const getAllProductByUser = `select * from products WHERE user_id=?`;
const getProductById = `select * FROM products WHERE id = ?`;
const getBidsByProductId = `select b.bid_amount, b.bid_qty, u.username, u.id as user_id, r.avgRating, u.firstname, u.lastname, u.address, u.contact FROM product_bids as b  INNER JOIN users u ON b.user_id=u.id AND b.product_id = ? LEFT JOIN(SELECT AVG(rating) AS avgRating, user_id as ratedUser FROM user_rating GROUP BY ratedUser) r ON r.ratedUser = b.user_id ORDER BY b.bid_amount DESC`
const createBid = `INSERT INTO product_bids  VALUES (null,?, ?, ?, ?, 'kg',NOW(),NOW())`;

export {
    createProduct,
    getAllProduct,
    getProductById,
    getBidsByProductId,
    createBid,
    getAllProductByUser
}