import { DB_NAME } from '../utils/secrets.js'

const createDB = `CREATE DATABASE IF NOT EXISTS ${DB_NAME}`;

const dropDB = `DROP DATABASE IF EXISTS ${DB_NAME}`;

const createTableUSers = `
CREATE TABLE IF NOT EXISTS users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    firstname VARCHAR(50) NULL,
    lastname VARCHAR(50) NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    created_on TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
)
`;
const createNewUser = `
INSERT INTO users VALUES(null,?, ?, ?, ?,?,?,?,NOW())
`;
//INSERT INTO `farmers`.`users` (`username`, `firstname`, `type`, `address`, `contact`, `password`) VALUES ('admin', 'admin', '1', 'admin', '1352235', 'sadfsf');


const findUserByUsername = `
SELECT * FROM users WHERE username = ?
`;

const findAllUserByUsername = `
SELECT * FROM users`;

const createRatingQuery = `INSERT INTO user_rating VALUES(null, ?, ?, ?, NOW())`;

const getUserAvgRating = `SELECT AVG(r.rating) AS avgRating, u.username, u.id as user_id, u.firstname, u.lastname, u.address, u.contact FROM user_rating r LEFT JOIN users u ON r.user_id = u.id GROUP BY r.user_id
`


export {
    createDB,
    dropDB,
    createTableUSers,
    createNewUser,
    findUserByUsername,
    findAllUserByUsername,
    createRatingQuery,
    getUserAvgRating
};
