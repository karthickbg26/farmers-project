import { logger } from '../../utils/logger.js';
import { createDB as createDBQuery } from '../queries.js';

(() => {
    require('../../config/db.config.init').query(createDBQuery, (err, _) => {
        if (err) {
            logger.error(err.message);
            return;
        }
        logger.info('DB created!');
        process.exit(0);
    });
})();
