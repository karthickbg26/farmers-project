import { logger }from ('../../utils/logger.js');
import { createTableUSers as createTableUSersQuery } from '../queries.js';

(() => {    
   require('../../config/db.config').query(createTableUSersQuery, (err, _) => {
        if (err) {
            logger.error(err.message);
            return;
        }
        logger.info('Table users created!');
        process.exit(0);
    });
})();
