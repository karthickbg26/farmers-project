import User from '../models/user.model.js';

const checkUsername =  (req, res, next) => {
    const { username } = req.body;
    User.findByUsername(username, (_, data) => {
        if (data) {
            res.status(400).send({
                status: 'error',
                message: `A user with username address '${username}' already exits`
            });
            return;
        }
        next();
    });
}

export default checkUsername;