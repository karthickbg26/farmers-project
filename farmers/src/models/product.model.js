import db from '../config/db.config.js';
import { createProduct, getAllProduct, getProductById, getBidsByProductId , createBid, getAllProductByUser} from '../database/productQueries.js';
import { logger } from '../utils/logger.js';

class Product {
    constructor(product_name, product_image, base_price, qty, base_qty, base_qty_type, qty_type, user_id) {
        this.product_name = product_name,
            this.product_image = product_image,
            this.base_price = base_price,
            this.base_qty = base_qty,
            this.qty = qty,
            this.base_qty_type = base_qty_type,
            this.qty_type = qty_type,
            this.user_id = user_id
    }
    static create(newProduct, cb) {
        try {
            db.query(createProduct,
                [
                    newProduct.product_name,
                    newProduct.product_image,
                    newProduct.base_price,
                    newProduct.base_qty,
                    newProduct.qty,
                    newProduct.base_qty_type,
                    newProduct.qty_type,
                    newProduct.user_id
                ], (err, res) => {
                    if (err) {
                        logger.error(err.message);
                        cb(err, null);
                        return;
                    }
                    cb(null, {
                        id: res.insertId,
                        product_name: newProduct.product_name,
                        product_image: newProduct.product_image,
                        base_price: newProduct.base_price,
                        base_qty: newProduct.base_qty,
                        qty: newProduct.qty,
                        base_qty_type: newProduct.base_qty_type,
                        qty_type: newProduct.qty_type,
                        user_id: newProduct.user_id,
                        isActive: newProduct.isActive
                    });
                });
        }
        catch (error) {
            console.log("error", error);
        }
    }

    static findAll(cb) {
        db.query(getAllProduct, (err, res) => {
            if (err) {
                logger.error(err.message);
                cb(err, null);
                return;
            }
            if (res.length) {
                cb(null, res);
                return;
            }
            cb(null, res);
        })
    }

    static findAllByUser(id, cb) {
        db.query(getAllProductByUser, id, (err, res) => {
            if (err) {
                logger.error(err.message);
                cb(err, null);
                return;
            }
            if (res.length) {
                cb(null, res);
                return;
            }
            cb(null, res);
        })
    }

    static find(id, cb) {
        db.query(getProductById, id, (err, res) => {
            if (err) {
                logger.error(err.message);
                cb(err, null);
                return;
            }
            if (res.length) {
                cb(null, res);
                return;
            }
            cb({ kind: "not_found" }, null);
        })
    }



    static productBid(productId, cb) {
        db.query(getBidsByProductId, productId, (err, res) => {
            if (err) {
                logger.error(err.message);
                cb(err, null);
                return;
            }
            if (res.length) {
                cb(null, res);
                return;
            }
            cb(null, []);
        })
    }

    static createBid(bidPayload, cb) {
        try {
            db.query(createBid,
                [
                    bidPayload.product_id, bidPayload.user_id, bidPayload.bid_amount, bidPayload.bid_qty, bidPayload.bid_qty_type
                ], (err, res) => {
                    if (err) {
                        logger.error(err.message);
                        cb(err, null);
                        return;
                    }
                    cb(null, {
                        id: res.insertId,
                        product_id: bidPayload.product_id,
                        user_id: bidPayload.user_id,
                        bid_amount: bidPayload.bid_amount,
                        bid_qty: bidPayload.bid_qty,
                        bid_qty_type: bidPayload.bid_qty_type
                    });
                });
        }
        catch (error) {
            console.log("error", error);
        }
    }
}


export default Product;