import db from '../config/db.config.js';
import { createNewUser, findUserByUsername, findAllUserByUsername, createRatingQuery,getUserAvgRating } from '../database/queries.js';
import { logger } from '../utils/logger.js';

class User {
    constructor(username, firstname, lastname, type, address, contact, password) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.type = type;
        this.address = address;
        this.contact = contact;
        this.password = password;
    }

    static create(newUser, cb) {
        try {

            db.query(createNewUser,
                [
                    newUser.username,
                    newUser.firstname,
                    newUser.lastname,
                    newUser.type,
                    newUser.address,
                    newUser.contact,
                    newUser.password,

                ], (err, res) => {
                    if (err) {
                        logger.error(err.message);
                        cb(err, null);
                        return;
                    }
                    cb(null, {
                        id: res.insertId,
                        username: newUser.username,
                        firstname: newUser.firstname,
                        lastname: newUser.lastname,
                        type: newUser.type,
                        address: newUser.address,
                        contact: newUser.contact,
                    });
                });
        }
        catch (error) {
            console.log("error", error);
        }
    }

    static findByUsername(username, cb) {
        db.query(findUserByUsername, username, (err, res) => {
            if (err) {
                logger.error(err.message);
                cb(err, null);
                return;
            }
            if (res.length) {
                cb(null, res[0]);
                return;
            }
            cb({ kind: "not_found" }, null);
        })
    }

    static findAllUsers(cb) {
        db.query(findAllUserByUsername, (err, res) => {
            if (err) {
                logger.error(err.message);
                cb(err, null);
                return;
            }
            if (res.length) {
                cb(null, res);
                return;
            }
            cb({ kind: "not_found" }, null);
        })
    }

    static createRating(newRate, cb) {
        try {

            db.query(createRatingQuery,
                [
                    newRate.user_id,
                    newRate.rating,
                    newRate.rating_user

                ], (err, res) => {
                    if (err) {
                        logger.error(err.message);
                        cb(err, null);
                        return;
                    }
                    cb(null, {
                        id: res.insertId,
                        rating: newRate.rating
                    });
                });
        }
        catch (error) {
            console.log("error", error);
        }
    }
    
    static averageRating(userId, cb) {
        db.query(getUserAvgRating, userId, (err, res) => {
            if (err) {
                logger.error(err.message);
                cb(err, null);
                return;
            }
            if (res.length) {
                cb(null, res);
                return;
            }
            cb({ kind: "not_found" }, null);
        })
    }
}

export default User;