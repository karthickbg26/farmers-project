import express from 'express';
import { asyncHandler } from '../middlewares/asyncHandler.js';
import checkUsername from '../middlewares/checkUsername.js';
import { signup as signupValidator, signin as signinValidator } from '../validators/auth.js';
import {signup, signin}  from '../controllers/auth.controller.js';

const router = express.Router();

router.route('/signup')
    .post(signupValidator, asyncHandler(checkUsername), asyncHandler(signup));

router.route('/signin')
    .post(signinValidator, asyncHandler(signinValidator), asyncHandler(signin));

export default router;