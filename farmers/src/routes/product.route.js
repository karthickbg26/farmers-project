import express from 'express';
import { asyncHandler } from '../middlewares/asyncHandler.js';
import { authenticateJWT } from '../utils/authenticateJWT.js';
import { create, findAll, find, productBid, createBid } from '../controllers/product.controller.js';

const router = express.Router();
router.route('/create')
    .post(authenticateJWT, asyncHandler(create));
router.route('/')
    .get(authenticateJWT, asyncHandler(findAll));
router.route('/:id')
    .get(authenticateJWT, asyncHandler(find));
router.route('/bid/:productId')
    .get(authenticateJWT, asyncHandler(productBid));
router.route('/bid/create')
    .post(authenticateJWT, asyncHandler(createBid));

export default router;
