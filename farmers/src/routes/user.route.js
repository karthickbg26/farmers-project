import express from 'express';
import { asyncHandler } from '../middlewares/asyncHandler.js';
import { authenticateJWT } from '../utils/authenticateJWT.js';
import {findAllUsers, createRating, averageRating} from '../controllers/user.controller.js';

const router = express.Router();
router.route('/')
    .get(authenticateJWT, asyncHandler(findAllUsers));
router.route('/rating')
    .post(authenticateJWT, asyncHandler(createRating));
router.route('/averageRating/:userId')
    .get(authenticateJWT, asyncHandler(averageRating));


export default router;