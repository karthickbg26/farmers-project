import bcrypt from 'bcryptjs';

const hash = (password) => bcrypt.hashSync(password, bcrypt.genSaltSync(10));

const compare = (password, hashedPassword) => bcrypt.compareSync(password, hashedPassword);

export {
    hash,
    compare
}