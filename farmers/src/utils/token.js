import jwt from 'jsonwebtoken';
import { JWT_SECRET_KEY } from '../utils/secrets.js';
import { logger } from './logger.js';

const generate = (data) => jwt.sign({ ...data }, JWT_SECRET_KEY, { expiresIn: '1d'});

const decode = (token) => {
    try {
        return jwt.verify(token, JWT_SECRET_KEY)
    } catch (error) {
        logger.error(error);
    }
};

export {
    generate,
    decode
}