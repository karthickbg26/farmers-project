import Joi from 'joi';
import validatorHandler from '../middlewares/validatorHandler.js';

const signup = (req, res, next) => {
    const schema = Joi.object().keys({
        username: Joi.string()
            .trim()
            .alphanum()
            .min(3)
            .max(50)
            .required(),
        firstname: Joi.string()
            .trim()
            .alphanum()
            .min(3)
            .max(50)
            .required(),
        lastname: Joi.string()
            .trim()
            .alphanum()
            .min(3)
            .max(50)
            .optional(),
        type: Joi.number()
            .valid(1, 2)
            .required(),
        address: Joi.string()
            .trim()
            .alphanum()
            .min(3)
            .max(50)
            .optional(),
        contact: Joi.string()
            .trim()
            .optional(),
        password: Joi.string()
            .trim()
            .min(6)
            .max(15)
            .required()
    });
    validatorHandler(req, res, next, schema);
};

const signin = (req, res, next) => {
    const schema = Joi.object().keys({
        username: Joi.string()
            .trim()
            .required(),
        password: Joi.string()
            .trim()
            .required()
    });
    validatorHandler(req, res, next, schema);
};

export {
    signup,
    signin
};