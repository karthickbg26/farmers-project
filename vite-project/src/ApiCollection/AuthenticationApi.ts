import { BASE_URL } from '../appConfig';
import { BaseRequestProvider } from './BaseRequestProvider';

const AuthAPIServiceInstance: any = null;
export default class AuthAPIService extends BaseRequestProvider {
  public routes: any = {
    login: {
      url: `${BASE_URL}/auth/signin`,
      method: 'POST',
    },
  };
  constructor() {
    super();
    if (AuthAPIServiceInstance) {
      return AuthAPIServiceInstance;
    }
  }

  async login(body: any) {
    const { login } = this.routes;
    const requestParams = {
        ...login,
        body,
    }
    return await this.sendRequest(requestParams);

  }

}
