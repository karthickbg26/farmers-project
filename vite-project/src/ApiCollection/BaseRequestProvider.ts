import isomorphicUnfetch from "isomorphic-unfetch";

interface IRequestParams {
  url: string;
  method: string;
  body?: any;
  authToken?: string;
}

interface IAPIResponse {
    message: string;
    data: any;
    status: number;
  }

const DEFAULT_HEADERS = {
  "Content-Type": "application/json",
};

export class BaseRequestProvider {
  async sendRequest(requestParams: IRequestParams): Promise<IAPIResponse> {
    const {
      url,
      method,
      body = null,
      authToken = '',
    } = requestParams;

    const headers: any = {
      ...DEFAULT_HEADERS,
    };
        
    headers["Authorization"] = `Bearer ${authToken}`;
    const payload: any = { method, headers };
    if (body) {
      payload.body = JSON.stringify(body);
    }
    return new Promise((resolve, reject) => {
      isomorphicUnfetch(url, payload)
        .then(async (response: any) => {
          const result = await response.json();
          resolve(result);
        })
        .catch((error) => reject(error));
    });
  }


  
}
