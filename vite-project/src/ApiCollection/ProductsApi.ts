import { BASE_URL } from '../appConfig';
import { BaseRequestProvider } from './BaseRequestProvider';

const ProductAPIServiceInstance: any = null;
export default class ProductAPIService extends BaseRequestProvider {
  public routes: any = {
    getProducts: {
      url: `${BASE_URL}/product`,
      method: 'GET',
    },
    addProduct: {
      url: `${BASE_URL}/product/create`,
      method: 'POST',
    },
    getProductBids: (id: string)=> ({
      url: `${BASE_URL}/product/bid/${id}`,
      method: 'GET',
    }),
    addBid: {
      url: `${BASE_URL}/product/bid/create`,
      method: 'POST',
    }
  };
  constructor() {
    super();
    if (ProductAPIServiceInstance) {
      return ProductAPIServiceInstance;
    }
  }

  async getProducts(authToken: string) {
    const { getProducts } = this.routes;
    const requestParams = {
        ...getProducts,
        authToken
    }
    return await this.sendRequest(requestParams);

  }

  async addProduct(authToken: string, body: any) {
    const { addProduct } = this.routes;
    const requestParams = {
        ...addProduct,
        body,
        authToken
    }
    return await this.sendRequest(requestParams);

  }

  async getBids(authToken: string, id: string) {
    const { getProductBids } = this.routes;
    const requestParams = {
        ...getProductBids(id),
        authToken
    }
    return await this.sendRequest(requestParams);

  }
  async addBid(authToken: string, body: any) {
    const { addBid } = this.routes;
    const requestParams = {
        ...addBid,
        body,
        authToken
    }
    return await this.sendRequest(requestParams);

  }    

}
