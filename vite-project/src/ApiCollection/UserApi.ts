import { BASE_URL } from '../appConfig';
import { BaseRequestProvider } from './BaseRequestProvider';

const UserAPIServiceInstance: any = null;
export default class UserAPIService extends BaseRequestProvider {
  public routes: any = {
    
    getUserRating: (id: string)=> ({
      url: `${BASE_URL}/users/averageRating/${id}`,
      method: 'GET',
    }),
  };
  constructor() {
    super();
    if (UserAPIServiceInstance) {
      return UserAPIServiceInstance;
    }
  }

  async getUserRating(authToken: string, id: string) {
    const { getUserRating } = this.routes;
    const requestParams = {
        ...getUserRating(id),
        authToken
    }
    return await this.sendRequest(requestParams);

  } 

}
