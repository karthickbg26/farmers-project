import { connect } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { IDispatch, IRootState } from '../store';

interface IProtectedRouteProps extends TStateProps{
    children: JSX.Element;
  }

export const ProtectedRoute = ({isLoggedIn, children}: IProtectedRouteProps) => {
    if (!isLoggedIn) {
      return <Navigate to="/login" replace />;
    }
  
    return children;
  };

  const mapState = (state: IRootState) => ({
    isLoggedIn: state.userSession.isLoggedIn,
  });
  
  const mapDispatch = (dispatch: IDispatch) => ({
    dispatch
  });
  
  type TStateProps = ReturnType<typeof mapState>;
  
  
  export default connect(mapState, mapDispatch)(ProtectedRoute);
  