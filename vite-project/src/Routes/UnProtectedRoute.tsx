import { connect } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { IDispatch, IRootState } from '../store';

interface IUnProtectedRouteProps extends TStateProps{
    children: JSX.Element;
  }

export const UnProtectedRoute = ({isLoggedIn, children}: IUnProtectedRouteProps) => {
    if (isLoggedIn) {
      return <Navigate to="/" replace />;
    }
  
    return children;
  };

  const mapState = (state: IRootState) => ({
    isLoggedIn: state.userSession.isLoggedIn,
  });
  
  const mapDispatch = (dispatch: IDispatch) => ({
    dispatch
  });
  
  type TStateProps = ReturnType<typeof mapState>;
  
  
  export default connect(mapState, mapDispatch)(UnProtectedRoute);
  