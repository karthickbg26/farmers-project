import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import PageNotFound from "../components/Common/PageNotFound";
import DashBoard from "../components/Pages/DashBoard";
import Login from "../components/Pages/Login";
import ProtectedRoute from "./ProtectedRoute";
import UnProtectedRoute from "./UnProtectedRoute";

export default () => (
    <Router>
      <Routes>
        <Route path="/" element={ 
          <ProtectedRoute>
            <DashBoard/>
          </ProtectedRoute> 
        } />
        <Route path="/dashboard" element={ 
          <ProtectedRoute>
            <DashBoard/>
          </ProtectedRoute> 
        } />
        <Route path="/login" element={ 
          <UnProtectedRoute>
            <Login/>
          </UnProtectedRoute> 
        } />
        <Route path="/page-not-found" element={ <PageNotFound/> } />
      </Routes>
    </Router>
  );