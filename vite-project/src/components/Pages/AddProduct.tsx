import React, { useReducer } from "react";
import SideBar from "../Common/SideBar";

interface IAddProducts {
    cb: (state: any)=>void;
}

const initialState = {
    product_name: '',
    product_image: '',
    base_price: '',
    base_qty: 1,
    base_qty_type:'kg',
    qty: null,
    qty_type: 'kg',
}

const reducer = (state: any, action: any) => {
    switch(action.type) {
        case 'name':
            return {...state, product_name: action.value}
        case 'image':
            return {...state, product_image: action.value}
        case 'base_price':
            return {...state, base_price: action.value}
        case 'qty':
            return {...state, qty: action.value}
        case 'qty_type':
            return {...state, qty_type: action.value}
        default:
            throw new Error();        
    }
}

const AddProduct = ({cb}: IAddProducts) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const onSave = () => {
        cb(state);
    }
    return (
                <div className="mt-10 sm:mt-0">
                    <div className="flex flex-col align-center p-10">
                        <div className="flex flex-row justify-between">
                            <div className="px-4 sm:px-0">
                                <h3 className="text-lg font-medium leading-6 text-gray-900">Product Information</h3>
                            </div>
                            <button
                                type="button"
                                className="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                                onClick={()=>cb(null)}
                            >
                                Go back
                            </button>
                        </div>
                        <div className="mt-5 md:col-span-2 md:mt-0">
                            <form action="#" method="POST">
                                <div className="overflow-hidden shadow sm:rounded-md">
                                    <div className="bg-white px-4 py-5 sm:p-6">
                                        <div className="grid grid-cols-6 gap-6">
                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="product_name" className="block text-sm font-medium text-gray-700">
                                                    Product name
                                                </label>
                                                <input
                                                    type="text"
                                                    name="product_name"
                                                    id="product_name"
                                                    value={state.product_name}
                                                    autoComplete="product_name"
                                                    onChange={(e) => dispatch({type: 'name',value: e.target.value})}
                                                    className="mt-1 block w-full rounded-md px-3 py-2 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="product_image" className="block text-sm font-medium text-gray-700">
                                                    Image URL
                                                </label>
                                                <input
                                                    type="text"
                                                    name="product_image"
                                                    id="product_image"
                                                    value={state.product_image}
                                                    onChange={(e) => dispatch({type: 'image',value: e.target.value})}
                                                    autoComplete="product_image"
                                                    className="mt-1 block w-full rounded-md px-3 py-2 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                                />
                                            </div>
                                            <div className="col-span-6 sm:col-span-2"></div>

                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="base_price" className="block text-sm font-medium text-gray-700">
                                                    Base Price / kg
                                                </label>
                                                <input
                                                    type="text"
                                                    name="base_price"
                                                    id="base_price"
                                                    value={state.base_price}
                                                    onChange={(e) => dispatch({type: 'base_price',value: e.target.value})}
                                                    autoComplete="base_price"
                                                    className="mt-1 block w-full rounded-md px-3 py-2 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-2">
                                                
                                            </div>
                                            <div className="col-span-6 sm:col-span-2"></div>

                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="qty" className="block text-sm font-medium text-gray-700">
                                                    Total Quantity
                                                </label>
                                                <input
                                                    type="number"
                                                    name="qty"
                                                    id="qty"
                                                    value={state.qty}
                                                    autoComplete="qty"
                                                    onChange={(e) => dispatch({type: 'qty',value: e.target.value})}
                                                    className="mt-1 block w-full rounded-md px-3 py-2 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                                />
                                            </div>
                                            <div className="col-span-6 sm:col-span-2">
                                                <label htmlFor="qty_type" className="block text-sm font-medium text-gray-700">
                                                    Quantity type
                                                </label>
                                                <select
                                                    id="qty_type"
                                                    name="qty_type"
                                                    autoComplete="qty_type"
                                                    value={state.qty_type}
                                                    onChange={(e) => dispatch({type: 'qty_type',value: e.target.value})}
                                                    className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                                                >
                                                    <option>Kg</option>
                                                    <option>Ton</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="bg-gray-50 px-4 py-3 text-right sm:px-6">
                                        <button
                                            type="button"
                                            className="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                                            onClick={onSave}
                                            disabled={state.product_name==='' || state.product_image==='' || state.base_price==='' || state.qty===null || state.qty === '0'} 
                                        >
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
    )
}

export default AddProduct;