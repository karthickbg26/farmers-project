import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import ProductAPIService from "../../ApiCollection/ProductsApi";
import { IDispatch, IRootState } from "../../store";
import SideBar from "../Common/SideBar";
import AddProduct from "./AddProduct";
import ViewProduct from "./ViewProduct";

const DashBoard = (props: TStateProps) => {
    const { userSession } = props;
    const ProductAPI = new ProductAPIService();
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [openAddProduct, setAddProduct] = useState(false);
    const [openSingleProduct, setOpenSingleProduct] = useState(false);
    const [product, setProduct] = useState(null);

    useEffect(() => {
        fetchProducts()
    }, [])
    const fetchProducts = () => {
        setIsLoading(true);
        ProductAPI.getProducts(userSession.token).then(res => {
            setProducts(res.data.data);
            setIsLoading(false);
        })
    }
    const addProduct = (state: any) => {
        setAddProduct(false);
        if(state) {
            setIsLoading(true);
            ProductAPI.addProduct(userSession.token, {...state, user_id: userSession.user_id}).then(res => {
                fetchProducts()
            })
        }
        
    }

    const openProduct = (selected: any) => {
        setProduct(selected);
        setOpenSingleProduct(true);
    }

    const closeProduct= () => {
        setOpenSingleProduct(false);
    }

    return (
        <SideBar>
            <div className="bg-white">
                { openAddProduct ? <AddProduct cb={addProduct}/> :  (openSingleProduct ? <ViewProduct cb={closeProduct} selectedProduct={product}/> :
                (<div className="mx-auto max-w-2xl py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
                    <div className="flex items-center justify-between">
                    <h2 className="text-2xl font-bold tracking-tight text-gray-900">Your products</h2>
                    {userSession.userType === 1 && <button
                        type="button"
                        className="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                        onClick={()=>setAddProduct(true)}
                    >
                        Add Products
                    </button>}
                    </div>

                    <div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                        {isLoading? (<button type="button" disabled>
                            <svg className="animate-spin h-5 w-5 mr-3 ..." viewBox="0 0 24 24">
                            </svg>
                            Processing...
                        </button>) : 
                        products.map((product: any) => (
                            <div 
                                key={product.id} 
                                className="group relative cursor-pointer" 
                                onClick={()=>openProduct(product)}
                            >
                                <div className="min-h-80 aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-md bg-gray-200 group-hover:opacity-75 lg:aspect-none lg:h-80">
                                    <img
                                        src={product?.product_image}
                                        alt={product.product_name}
                                        className="h-full w-full object-cover object-center lg:h-full lg:w-full cursor-pointer"
                                    />
                                </div>
                                <div className="mt-4 flex justify-between">
                                    <div>
                                        <h3 className="text-sm text-gray-700">
                                            <a href={product.href}>
                                                <span aria-hidden="true" className="absolute inset-0" />
                                                {product.product_name}
                                            </a>
                                        </h3>
                                        <p className="mt-1 text-sm text-gray-500">{product.qty} {product.qty_type}</p>
                                    </div>
                                    <p className="text-sm font-medium text-gray-900">{product.base_price} / {product.base_qty_type}</p>
                                </div>
                            </div>
                        ))}
                        
                    </div>
                </div>))
                }
            </div>
        </SideBar>
    )
}
const mapState = (state: IRootState) => ({
    userSession: state.userSession
  });
  
  const mapDispatch = (dispatch: IDispatch) => ({});
  
  type TStateProps = ReturnType<typeof mapState>;
  
  
  export default connect(mapState, mapDispatch)(DashBoard);