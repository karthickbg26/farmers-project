import React, { useState } from "react";
import SideBar from "../Common/SideBar";
import { Avatar, ChatContainer, Conversation, ConversationHeader, ConversationList, EllipsisButton, MainContainer, Message, MessageInput, MessageList, MessageSeparator, Search, Sidebar, TypingIndicator, VideoCallButton, VoiceCallButton } from "@chatscope/chat-ui-kit-react";
import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
const zoeIco = "/vite.svg";
const Teams = () =>
{
    const [messageInputValue, setMessageInputValue] = useState('');
    return(
        <SideBar>
            <main className="flex-1">
                <div className="py-6">
                    <div className="mx-auto max-w-7xl px-4 sm:px-6 md:px-8">
                        <h1 className="text-2xl font-semibold text-gray-900">#Teams</h1>
                        <div style={{
                            height: "600px",
                            position: "relative"
                        }}>
                            <MainContainer responsive>                
                            <Sidebar position="left" scrollable={false}>
                                <Search placeholder="Search..." />
                                <ConversationList>                                                     
                                    <Conversation name="Lilly" lastSenderName="Lilly" info="Yes i can do it for you" active={true}>
                                        <Avatar src={zoeIco} name="Lilly" status="available" />
                                    </Conversation>
                                    
                                    <Conversation name="Joe" lastSenderName="Joe" info="Yes i can do it for you">
                                        <Avatar src={zoeIco} name="Joe" status="dnd" />
                                    </Conversation>
                                    
                                    <Conversation name="Emily" lastSenderName="Emily" info="Yes i can do it for you" unreadCnt={3}>
                                        <Avatar src={zoeIco} name="Emily" status="available" />
                                    </Conversation>
                                    
                                    <Conversation name="Kai" lastSenderName="Kai" info="Yes i can do it for you" unreadDot>
                                        <Avatar src={zoeIco} name="Kai" status="unavailable" />
                                    </Conversation>
                                                
                                    <Conversation name="Akane" lastSenderName="Akane" info="Yes i can do it for you">
                                        <Avatar src={zoeIco} name="Akane" status="eager" />
                                    </Conversation>
                                                        
                                    <Conversation name="Eliot" lastSenderName="Eliot" info="Yes i can do it for you">
                                        <Avatar src={zoeIco} name="Eliot" status="away" />
                                    </Conversation>
                                                                        
                                    <Conversation name="Zoe" lastSenderName="Zoe" info="Yes i can do it for you">
                                        <Avatar src={zoeIco} name="Zoe" status="dnd" />
                                    </Conversation>
                                    
                                    <Conversation name="Patrik" lastSenderName="Patrik" info="Yes i can do it for you">
                                        <Avatar src={zoeIco} name="Patrik" status="invisible" />
                                    </Conversation>                                 
                                </ConversationList>
                            </Sidebar>
                            
                            <ChatContainer>
                                <ConversationHeader>
                                <ConversationHeader.Back />
                                <Avatar src={zoeIco} name="Zoe" />
                                <ConversationHeader.Content userName="Zoe" info="Active 10 mins ago" />
                                <ConversationHeader.Actions>
                                    <VoiceCallButton />
                                    <VideoCallButton />
                                    <EllipsisButton orientation="vertical" />
                                </ConversationHeader.Actions>          
                                </ConversationHeader>
                                <MessageList typingIndicator={<TypingIndicator content="Zoe is typing" />}>
                                <MessageSeparator content="Saturday, 30 November 2019" />
                                <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Zoe",
                                        direction: "incoming",
                                        position: "single"
                                    }}>
                                                <Avatar src={zoeIco} name="Zoe" />
                                            </Message>
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Patrik",
                                        direction: "outgoing",
                                        position: "single"
                                    }} avatarSpacer />
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Zoe",
                                        direction: "incoming",
                                        position: "first"
                                    }} avatarSpacer />
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Zoe",
                                        direction: "incoming",
                                        position: "normal"
                                    }} avatarSpacer />
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Zoe",
                                        direction: "incoming",
                                        position: "normal"
                                    }} avatarSpacer />
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Zoe",
                                        direction: "incoming",
                                        position: "last"
                                    }}>
                                                <Avatar src={zoeIco} name="Zoe" />
                                            </Message>
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Patrik",
                                        direction: "outgoing",
                                        position: "first"
                                    }} />
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Patrik",
                                        direction: "outgoing",
                                        position: "normal"
                                    }} />
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Patrik",
                                        direction: "outgoing",
                                        position: "normal"
                                    }} />
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Patrik",
                                        direction: "outgoing",
                                        position: "last"
                                    }} />
                                            
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Zoe",
                                        direction: "incoming",
                                        position: "first"
                                    }} avatarSpacer />
                                            <Message model={{
                                        message: "Hello my friend",
                                        sentTime: "15 mins ago",
                                        sender: "Zoe",
                                        direction: "incoming",
                                        position: "last"
                                    }}>
                                    <Avatar src={zoeIco} name="Zoe" />
                                </Message>
                                </MessageList>
                                <MessageInput placeholder="Type message here" value={messageInputValue} onChange={val => setMessageInputValue(val)} />
                            </ChatContainer>                         
                            </MainContainer>
                        </div>
                    </div>
                </div>
            </main>
        </SideBar>)
}
export default Teams;