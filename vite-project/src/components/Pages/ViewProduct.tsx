import { Combobox } from "@headlessui/react";
import { ChevronRightIcon, UsersIcon } from "@heroicons/react/24/outline";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import ProductAPIService from "../../ApiCollection/ProductsApi";
import UserAPIService from "../../ApiCollection/UserApi";
import { IDispatch, IRootState } from "../../store";
import RatingComponent from "../Common/RatingComponent";

interface IViewProduct extends TStateProps {
    selectedProduct: any;
    cb: ()=>void;
}

function classNames(...classes: any) {
    return classes.filter(Boolean).join(' ')
  }

const ViewProduct = (props: IViewProduct) => {
    const { userSession, selectedProduct, cb } = props;
    const ProductAPI = new ProductAPIService();
    const UsersAPI = new UserAPIService();
    const [bidAmount, setBidAmount] = useState('');
    const [bidQty, setBidQty] = useState('');
    const [bids, setBids] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [product, setProduct] = useState(selectedProduct);
    const [productUserDetails, setDetails] = useState({
        avgRating:'',
        username:'',
        firstname:'',
        address:'',
        contact:'',
    });

    useEffect(() => {
        fetchBids();
        UsersAPI.getUserRating(userSession.token, userSession.user_id).then(res => {
            setDetails(res.data.data[0]);
        })
    }, [])
    const fetchBids = () => {
        setIsLoading(true);
        ProductAPI.getBids(userSession.token, product.id).then(res => {
            setBids(res.data.data);
            setIsLoading(false);
        })
    }

    const addBid = () => {
        const payload = {
            product_id: product.id,
            user_id: userSession.user_id,
            bid_amount: bidAmount,
            bid_qty: bidQty,
            bid_qty_type:'kg',
        }
        ProductAPI.addBid(userSession.token, payload).then(res => fetchBids())
    }

    return (
                <div className="mx-auto max-w-2xl py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
                    <div className="flex items-center justify-between">
                    <h2 className="text-2xl font-bold tracking-tight text-gray-900">{product.product_name}</h2>
                    <button
                        type="button"
                        className="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                        onClick={cb}
                    >
                        Go back
                    </button>
                    </div>

                    <div className="flex flex-col align-center">
                        <div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                            <div key={product.id} className="group relative">
                                <div className="min-h-80 aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-md bg-gray-200 group-hover:opacity-75 lg:aspect-none lg:h-80">
                                    <img
                                        src={product?.product_image}
                                        alt={product.product_name}
                                        className="h-full w-full object-cover object-center lg:h-full lg:w-full"
                                    />
                                </div>
                                
                            </div>
                            <div className="mt-4 flex-row justify-between">
                                <div className="flex justify-between">
                                    <div>
                                        <h3 className="text-sm text-gray-700">
                                            <a href={product.href}>
                                                <span aria-hidden="true"/>
                                                {product.product_name}
                                            </a>
                                        </h3>
                                        <p className="mt-1 text-sm text-gray-500">{product.qty} {product.qty_type}</p>
                                    </div>
                                    <p className="text-sm font-medium text-gray-900">₹{product.base_price} / {product.base_qty_type}</p>
                                </div>
                                <div>
                                    <h1 className="text-sm text-gray-700">Seller info</h1>
                                    <div className="hidden h-96 w-full flex-none flex-col divide-y divide-gray-100 overflow-y-auto sm:flex">
                                        <div className="flex-none p-6 text-center">
                                        <span className="inline-block h-14 w-14 overflow-hidden rounded-full bg-gray-100">
                                            <svg className="h-full w-full text-gray-300" fill="currentColor" viewBox="0 0 24 24">
                                            <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                                            </svg>
                                        </span>
                                        <h2 className="mt-3 font-semibold text-gray-900">{productUserDetails.firstname}</h2>
                                        <div className="flex justify-center">
                                            <RatingComponent star={productUserDetails.avgRating}/>
                                        </div>
                                        </div>
                                        <div className="flex flex-auto flex-col justify-between p-6">
                                        <dl className="grid grid-cols-1 gap-x-6 gap-y-3 text-sm text-gray-700">
                                            <dt className="col-end-1 font-semibold text-gray-900">Phone</dt>
                                            <dd>{productUserDetails.contact}</dd>
                                            
                                            <dt className="col-end-1 font-semibold text-gray-900">Address</dt>
                                            <dd className="truncate">
                                            {productUserDetails.address}
                                            </dd>
                                        </dl>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>

                        {isLoading? (<button type="button" disabled>
                            <svg className="animate-spin h-5 w-5 mr-3 ..." viewBox="0 0 24 24">
                            </svg>
                            Processing...
                        </button>) : null}
                        <div>
                        {userSession.userType  === 2 &&  !bids.some((bid: any) => bid.user_id === userSession.user_id) && 
                                    <div className="grid grid-cols-6 gap-6">
                                         <div className="col-span-6 sm:col-span-2"> 
                                            <input
                                                type="text"
                                                name="bid_amount"
                                                id="bid_amount"
                                                placeholder="Bid amount"
                                                value={bidAmount}
                                                onChange={(e) => setBidAmount(e.target.value)}
                                                autoComplete="bid_amount"
                                                className="mt-1 block w-full rounded-md px-3 py-2 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                            />
                                        </div>
                                        <div className="col-span-6 sm:col-span-2"> 
                                        <input
                                            type="number"
                                            name="bidQty"
                                            id="bidQty"
                                            placeholder="quantity (kg)"
                                            value={bidQty}
                                            onChange={(e) => setBidQty(e.target.value)}
                                            autoComplete="bidQty"
                                            className="mt-1 block w-full rounded-md px-3 py-2 border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                                        />
                                        </div>
                                        <div className="col-span-6 sm:col-span-2"> 
                                        <button
                                            type="button"
                                            className="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:text-sm"
                                            disabled={bidAmount === '' || bidQty === ''}
                                            onClick={addBid}
                                        >
                                            Add your bid
                                        </button>
                                        </div>
                                    </div>
                                    }
                        </div>
                        {bids.length ? (<Combobox>
                            {({ activeOption }: any) => (
                            <div className="mt-6">  
                                <div className="flex flex-col justify-between w-full">
                                    <h2 className="text-2xl font-bold tracking-tight text-gray-900">Bids</h2>
                                </div>
                                <Combobox.Options as="div" static hold className="flex divide-x divide-gray-100 mt-6">
                                    <>
                                    <div
                                    className={classNames(
                                        'max-h-96 min-w-0 flex-auto scroll-py-4 overflow-y-auto px-6 py-4',
                                        activeOption && 'sm:h-96'
                                    )}
                                    >
                                    <div className="-mx-2 text-sm text-gray-700">
                                        {bids.map((bid: any, index) => (
                                        <Combobox.Option
                                            as="div"
                                            key={index}
                                            value={bid}
                                            className={({ active }) =>
                                            classNames(
                                                'flex cursor-default select-none items-center rounded-md p-2',
                                                active && 'bg-gray-100 text-gray-900'
                                            )
                                            }
                                        >
                                            {({ active }) => (
                                            <>
                                                <span className="inline-block h-8 w-8 overflow-hidden rounded-full bg-gray-100">
                                                    <svg className="h-full w-full text-gray-300" fill="currentColor" viewBox="0 0 24 24">
                                                    <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                                                    </svg>
                                                </span>
                                                <span className="ml-3 flex-auto truncate">{bid.firstname}  ( ₹{bid.bid_amount})</span>
                                                {active && (
                                                <ChevronRightIcon
                                                    className="ml-3 h-5 w-5 flex-none text-gray-400"
                                                    aria-hidden="true"
                                                />
                                                )}
                                            </>
                                            )}
                                        </Combobox.Option>
                                        ))}
                                    </div>
                                    </div>

                                    {activeOption && (
                                    <div className="hidden h-96 w-1/2 flex-none flex-col divide-y divide-gray-100 overflow-y-auto sm:flex">
                                        <div className="flex-none p-6 text-center">
                                        <span className="inline-block h-16 w-16 overflow-hidden rounded-full bg-gray-100">
                                            <svg className="h-full w-full text-gray-300" fill="currentColor" viewBox="0 0 24 24">
                                            <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                                            </svg>
                                        </span>
                                        <h2 className="mt-3 font-semibold text-gray-900">{activeOption.firstname}</h2>
                                        <div className="flex justify-center">
                                            <RatingComponent star={activeOption.avgRating}/>
                                        </div>
                                        </div>
                                        <div className="flex flex-auto flex-col justify-between p-6">
                                        <dl className="grid grid-cols-1 gap-x-6 gap-y-3 text-sm text-gray-700">
                                            <dt className="col-end-1 font-semibold text-gray-900">Phone</dt>
                                            <dd>{activeOption.contact}</dd>
                                            
                                            <dt className="col-end-1 font-semibold text-gray-900">Address</dt>
                                            <dd className="truncate">
                                            {activeOption.address}
                                            </dd>

                                            <dt className="col-end-1 font-semibold text-gray-900">Bid amount / kg</dt>
                                            <dd className="truncate">
                                            ₹{activeOption.bid_amount}
                                            </dd>

                                            <dt className="col-end-1 font-semibold text-gray-900">Bid Quantity</dt>
                                            <dd className="truncate">
                                            {activeOption.bid_qty}
                                            </dd>
                                        </dl>
                                        {userSession.userType===1 && <button
                                            type="button"
                                            className="mt-6 w-full rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                                        >
                                            Send message
                                        </button>}
                                        </div>
                                    </div>
                                    )}
                                    </>
                                </Combobox.Options>
                            </div>
                            )}
                        </Combobox>) : <div className="w-full text-center mt-10">No bids yet</div>}
                    </div>
                </div>
    )
}
const mapState = (state: IRootState) => ({
    userSession: state.userSession
  });
  
  const mapDispatch = (dispatch: IDispatch) => ({});
  
  type TStateProps = ReturnType<typeof mapState>;
  
  
  export default connect(mapState, mapDispatch)(ViewProduct);