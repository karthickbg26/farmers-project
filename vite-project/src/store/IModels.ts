import {
    sideBarStore,
    userSession,
} from './models';

export interface RootModel {
    sideBarStore : typeof sideBarStore;
    userSession: typeof userSession;
}

export const models: RootModel = {
    sideBarStore,
    userSession,
}