import { IRootState } from "..";


const initialState = {
    selectedNavItem: 'Dashboard',
}

export type ISideBarStore = typeof initialState;

const sideBarStore = {
    state: <ISideBarStore> initialState,
    reducers: {
        update(state: ISideBarStore, payload: ISideBarStore): ISideBarStore {
                return payload
        },
    },
    effects: {
        update(payload: any, state: IRootState) {
            //use for effects
        },
    },
};

export default sideBarStore;
