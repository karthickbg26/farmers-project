import { IRootState, store } from "..";

export interface IUserSession {
    isLoggedIn: boolean;
    userName: string;
    token: string;
    user_id: string;
    firstname: string;
    lastname: string;
    userType: number;
}

const initialState: IUserSession = {
    isLoggedIn: false,
    userName: '',
    token: '',
    user_id:'',
    firstname:'',
    lastname:'',
    userType: 0,
};
const userSession = {
    state: <IUserSession>initialState,
    reducers: {
        update(state: IUserSession, payload: Partial<IUserSession>): IUserSession {
            return { ...state, ...payload };
        },
        logout(state: IUserSession): IUserSession {
            return {
                ...initialState,
            };
        },
    },
    effects: {},
};

export default userSession;